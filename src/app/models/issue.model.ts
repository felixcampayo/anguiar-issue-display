import { ListItemInterface } from '../components/lists/interface/list-item.interface';
import { IssueEntity } from '../entities/issue.entity';
import { Injector, ReflectiveInjector } from '@angular/core';

/**
 * Issue Model
 */
export class IssueModel implements ListItemInterface {

  constructor(public issue: IssueEntity) {
  }

  /**
   * Returns main text
   */
  getText(): string {
    return this.issue.title;
  }

  /**
   * Returns open/close img
   */
  getImg(): string {
    let ret: string;
    if (this.issue.locked) {
      ret = 'assets/img/close.png';
    } else {
      ret = 'assets/img/open.png';
    }
    return ret;
  }

  /**
   * Returns issue id
   */
  getId(): string {
    return this.issue.id.toString();
  }

  /**
   * Returns created date
   */
  getDate(): string {
    return this.issue.created_at;
  }

  /**
   * Returns login user
   */
  getUser(): string {
    return this.issue.user.login;
  }
}


