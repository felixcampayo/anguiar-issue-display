import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { FlowData } from 'app/entities/flowData.entity';

@Injectable()
export class FlowService {

  private data: any; // Flow data

  constructor(
    private router: Router
  ) {
    this.data = {};
  }

  /**
   * Returns data from the flow
   * @param uri
   */
  getAndCleanData(uri: string): FlowData {
    let ret: FlowData;

    if (this.data[uri]) {
      ret = Object.assign({}, this.data[uri]);
      delete this.data[uri];
    }

    return ret;
  }

  /**
   * Save current flow data and navigate to next screen
   * @param uri
   * @param data
   */
  navigate(uri: string, data?: FlowData): void {
    if (data !== undefined) {
      this.data[uri] = data;
    }
    this.router.navigate([uri]);
  }
}
