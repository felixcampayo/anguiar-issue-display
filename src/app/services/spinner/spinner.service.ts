import { Injectable } from '@angular/core';

@Injectable()
export class SpinnerService {

    private show: boolean; // Show spinner flag

    constructor() {
        this.show = true;
    }

    /**
     * Returns show spinner flag
     */
    getShowSpinner(): boolean {
        return this.show;
    }

    /**
     * Show spinner
     */
    showSpinner(): void {
        this.show = true;
    }

    /**
     * Hide spinner
     * @param flag
     */
    hideSpinner(): void {
        this.show = false;
    }
}
