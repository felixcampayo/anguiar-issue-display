import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';

@Injectable()
export class HttpService {

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Returns list of items
   */
  getList(url: string): Observable<any> {
    return this.http.get<any>(url, { observe: 'response' });
  }
}
