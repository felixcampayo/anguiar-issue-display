import { Injector } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/if';
import { HttpService } from '../http/http.service';

/**
 * Iterator for inifinite scroll
 */
export class IteratorService {

  private firstUrl: string;
  private nextUrl: string;
  private previousUrl: string;
  private lastUrl: string;
  private currentUrl: string;

  private readonly firstLink: string = 'rel="first"';
  private readonly nextLink: string = 'rel="next"';
  private readonly previousLink: string = 'rel="prev"';
  private readonly lastLink: string = 'rel="last"';

  private httpService: HttpService;

  constructor(
    private injector: Injector,
    private urlRoot: string) {
    this.httpService = this.injector.get(HttpService);
    this.firstUrl = urlRoot;
    this.currentUrl = urlRoot;
  }

  /**
   * True if the pagination can go to the first page
   */
  hasFirst(): boolean {
    return (this.firstUrl !== undefined);
  }

  /**
   * True if the pagination can go to the next page
   */
  hasNext(): boolean {
    return (this.nextUrl !== undefined);
  }

  /**
  * True if the pagination can go to the previous page
  */
  hasPrevious(): boolean {
    return (this.previousUrl !== undefined);
  }

  /**
  * True if the pagination can go to the previous page
  */
  hasLast(): boolean {
    return (this.lastUrl !== undefined);
  }

  /**
   * Returns first page
   */
  first(): Observable<any> {
    return Observable.if(() => this.hasFirst() || this.urlRoot !== undefined,
      this.fetchData(this.firstUrl || this.urlRoot));
  }

  /**
   * Returns next page
   */
  next(): Observable<any> {
    return Observable.if(() => this.hasNext(),
      this.fetchData(this.nextUrl));
  }

  /**
   * Returns previous page
   */
  previous(): Observable<any> {
    return Observable.if(() => this.hasPrevious(),
      this.fetchData(this.previousUrl));
  }

  /**
   * Returns last page
   */
  last(): Observable<any> {
    return Observable.if(() => this.hasLast(),
      this.fetchData(this.lastUrl));
  }

  /**
   * Returns current page
   */
  current(): Observable<any> {
    return this.fetchData(this.currentUrl);
  }

  /**
   * Fetch data from the service
   */
  private fetchData(url: string): Observable<any[]> {
    return new Observable<any[]>((observer) => {
      this.httpService.getList(url)
        .subscribe((response: any) => {
          this.updateLinks(response.headers.get('Link'), url);
          observer.next(response.body);
          observer.complete();
        }, (error) => observer.error({
          title: 'Error',
          text: 'Unexpected error'
        }));
    });
  }

  /**
   * Update links
   * @param header
   */
  private updateLinks(header: string, currentUrl: string): void {
    // Reset links
    this.firstUrl = undefined;
    this.nextUrl = undefined;
    this.previousUrl = undefined;
    this.lastUrl = undefined;

    // Update links
    this.currentUrl = currentUrl;

    if (this.hasLink(header, this.firstLink)) {
      this.firstUrl = this.getLink(header, this.firstLink);
    }

    if (this.hasLink(header, this.nextLink)) {
      this.nextUrl = this.getLink(header, this.nextLink);
    }

    if (this.hasLink(header, this.previousLink)) {
      this.previousUrl = this.getLink(header, this.previousLink);
    }

    if (this.hasLink(header, this.lastLink)) {
      this.lastUrl = this.getLink(header, this.lastLink);
    }
  }

  /**
   * Returns true if next link is in header
   */
  private hasLink(header: string, link: string): boolean {
    let ret: boolean;

    if (header) {
      const aux = header.match(link);
      ret = (aux !== null && aux.length !== 0);
    } else {
      ret = false;
    }

    return ret;
  }

  /**
   * Returns the new link in the header
   */
  private getLink(header: string, link: string): string {
    let ret: string;

    const aux = header.split(',');
    for (let i = 0; i < aux.length; i++) {
      if (this.hasLink(aux[i], link)) {
        ret = aux[i].split(';')[0].replace(/<|>/g, '');
      }
    }

    return ret;
  }
}
