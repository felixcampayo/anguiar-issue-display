import { Injectable } from '@angular/core';

@Injectable()
export class DialogService {

    private show: boolean; // Show dialog flag
    private title: string; // Title
    private text: string;  // Text

    constructor() {
        this.show = false;
        this.text = '';
        this.title = '';
    }

    /**
     * Returns show dialog flag
     */
    getShowDialog(): boolean {
        return this.show;
    }

    /**
     * Show dialog
     */
    showDialog(title: string, text: string): void {
        this.title = title;
        this.text = text;
        this.show = true;
    }

    /**
     * Hide dialog flag
     */
    hideDialog(): void {
        this.show = false;
    }

    /**
     * Returns title
     */
    getTitle(): string {
        return this.title;
    }

    /**
     * Returns text
     */
    getText(): string {
        return this.text;
    }
}
