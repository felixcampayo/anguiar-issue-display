import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ListItemInterface } from '../interface/list-item.interface';

@Component({
  selector: 'app-list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.scss'],
  providers: [DatePipe]
})
export class ListItemComponent implements OnInit {

  @Input() item: ListItemInterface; // ListItemInterface object
  isReady: boolean;                 // Component ready flag

  constructor(private datePipe: DatePipe) {
    this.isReady = false;
  }

  /**
   * Angular OnInit function. When all components are ready to display we set isReady flag to true
   */
  ngOnInit() {
    this.isReady = true;
  }

  getDetail(): string {
    return '#' + this.item.getId() + ' opened ' + this.datePipe.transform(this.item.getDate(), 'dd-MM-yyyy') + ' by ' + this.item.getUser();
  }
}
