import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ListItemInterface } from '../interface/list-item.interface';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {

  @Input() list: ListItemInterface[];         // List of elements to display
  @Input() title: string;                     // Title of the list
  @Output() selectedItem: EventEmitter<any>;  // Event emitter when the user selects one element

  isReady: boolean;                           // Component ready flag

  constructor() {
    this.isReady = false;
    this.selectedItem = new EventEmitter<any>();
  }

  /**
   * We display the whole template
   */
  ngOnInit() {
    this.isReady = true;
  }

  /**
   * Emits the selected item
   * @param item
   */
  selectedItemOutput(item: any): void {
    this.selectedItem.emit(item);
  }
}
