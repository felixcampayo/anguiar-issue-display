/**
 * List interface
 */
export interface ListItemInterface {
    getText(): string;
    getImg(): string;
    getId(): string;
    getUser(): string;
    getDate(): string;
}
