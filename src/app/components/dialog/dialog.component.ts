import { Component, OnInit, Input } from '@angular/core';
import { DialogService } from '../../services/dialog/dialog.service'

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {

  @Input() title: string;       // Title
  @Input() text: string;        // Main text of the dialog
  @Input() dialogShow: boolean; // Flag to show the dialog
  isReady: boolean;             // Component ready flag

  constructor(
    private dialogService: DialogService
  ) {
    this.isReady = false;
  }

  /**
   * Angular OnInit function. When all components are ready to display we set isReady flag to true
   */
  ngOnInit() {
    this.isReady = true;
  }

  /**
   * Close dialog event
   */
  closeClick() {
    this.dialogService.hideDialog();
  }
}
