import { IteratorService } from '../services/iterator/iterator.service';

/**
 * FlowData Entity
 */
export interface FlowData {
  data: any;
  pagination?: IteratorService;
}
