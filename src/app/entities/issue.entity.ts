import { UserEntity } from './user.entity';
import { LabelEntity } from './label.entity';
import { AssigneeEntity } from './assignee.entity';

/**
 * Issue Entity
 */
export interface IssueEntity {
  url?: string;
  repository_url?: string;
  labels_url?: string;
  comments_url?: string;
  events_url?: string;
  html_url?: string;
  id?: number;
  number?: number;
  title?: string;
  user?: UserEntity;
  labels?: LabelEntity[];
  state?: string;
  locked?: boolean;
  assignee?: AssigneeEntity;
  assignees?: AssigneeEntity[];
  milestone?: string;
  comments?: number;
  created_at?: string;
  updated_at?: string;
  closed_at?: string;
  author_association?: string;
  body?: string;
};
