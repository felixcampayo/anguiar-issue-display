export type PaginationType = 'first' | 'prev' | 'next' | 'last' | 'current';

/**
 * Pagination struct
 */
export const Pagination = {
  First: 'first' as PaginationType,
  Prev: 'prev' as PaginationType,
  Next: 'next' as PaginationType,
  Last: 'last' as PaginationType,
  Current: 'current' as PaginationType
};
