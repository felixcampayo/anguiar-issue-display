/**
 * Label Entity
 */
export interface LabelEntity {
  id?: number;
  url?: string;
  name?: string;
  color?: string;
  default?: boolean;
};
