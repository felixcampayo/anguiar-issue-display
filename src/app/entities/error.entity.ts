/**
 * Error Entity
 */
export interface ErrorEntity {
  title: string;
  text: string;
};
