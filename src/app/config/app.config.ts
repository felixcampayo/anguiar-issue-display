
// Host url
export const AppHost = 'https://api.github.com/'

// App configuration
export const AppConfig = {
    services: {
        issues: {
            getListOfIssues: AppHost + 'repos/angular/angular/issues?state=all'
        }
    },
    errors: {
        title: 'Warning',
        text: 'Unexpected error'
    },
    timeout: 2000
}
