import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './modules/app-routing.module';
import { DataListModule } from 'primeng/primeng';
import { ButtonModule } from 'primeng/primeng';
import { SpinnerComponentModule } from 'ng2-component-spinner';
import { DialogModule } from 'primeng/primeng';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { HttpService } from './services/http/http.service';
import { SpinnerService } from './services/spinner/spinner.service';
import { DialogService } from './services/dialog/dialog.service';
import { FlowService } from './services/flow/flow.service';

import { AppComponent } from './app.component';
import { DashboardComponent } from './pages/dashboard/dashboard.component';
import { IssueComponent } from './pages/issue/issue.component';
import { ListComponent } from './components/lists/list/list.component';
import { ListItemComponent } from './components/lists/list-item/list-item.component';
import { DialogComponent } from './components/dialog/dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    ListComponent,
    ListItemComponent,
    DialogComponent,
    IssueComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    DataListModule,
    ButtonModule,
    SpinnerComponentModule,
    DialogModule
  ],
  providers: [
    HttpService,
    SpinnerService,
    DialogService,
    FlowService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
