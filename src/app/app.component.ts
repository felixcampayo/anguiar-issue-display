import { Component } from '@angular/core';
import { SpinnerService } from './services/spinner/spinner.service';
import { DialogService } from './services/dialog/dialog.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {

  spinner: SpinnerService;  // Spinner service to be accessible in the template
  dialog: DialogService;    // Dialog service to be accessible in the template

  constructor(
    private spinnerService: SpinnerService,
    private dialogService: DialogService
  ) {
      this.spinner = this.spinnerService;
      this.dialog = this.dialogService;
  }
}
