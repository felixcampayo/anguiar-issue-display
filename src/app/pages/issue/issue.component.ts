import { Component } from '@angular/core';
import { AppConfig } from '../../config/app.config';
import { FlowService } from '../../services/flow/flow.service';
import { FlowData } from '../../entities/flowData.entity';
import { IssueModel } from '../../models/issue.model';

@Component({
  selector: 'app-issue',
  templateUrl: './issue.component.html',
  styleUrls: ['./issue.component.scss']
})
export class IssueComponent {

  data: IssueModel; // Issue Model
  flowData: FlowData;

  constructor(
    private flowService: FlowService
  ) {
    // Get data from the flow
    this.flowData = this.flowService.getAndCleanData('/issue');

    // If data is undefined go back to dashboard
    if (!this.flowData) {
      this.flowService.navigate('/dashboard');
    } else {
      this.data = this.flowData.data;
    }
  }

  /**
   * Go back to dashboard
   */
  goBackClick(): void {
    this.flowService.navigate('/dashboard', { data: undefined, pagination: this.flowData.pagination });
  }
}
