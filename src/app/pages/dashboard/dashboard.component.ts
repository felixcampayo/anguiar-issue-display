import { Component, OnDestroy, Injector } from '@angular/core';
import { DatePipe } from '@angular/common';
import { IteratorService } from '../../services/iterator/iterator.service';
import { HttpService } from '../../services/http/http.service';
import { SpinnerService } from '../../services/spinner/spinner.service';
import { DialogService } from '../../services/dialog/dialog.service';
import { AppConfig } from '../../config/app.config';
import { IssueModel } from 'app/models/issue.model';
import { IssueEntity } from '../../entities/issue.entity';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/finally';
import { Pagination, PaginationType } from '../../entities/pagination.entity';
import { ErrorEntity } from 'app/entities/error.entity';
import { FlowService } from '../../services/flow/flow.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnDestroy {

  issuesList: IssueModel[];         // Issue list
  isReady: boolean;                 // Component ready flag
  iteratorService: IteratorService; // Iterator for the list
  pagination: any;                  // Pagination steps

  private timeout: any;             // Timeout pointer

  constructor(
    private injector: Injector,
    private spinnerService: SpinnerService,
    private dialogService: DialogService,
    private flowService: FlowService
  ) {
    this.isReady = false;
    this.pagination = Pagination;

    // Get data from the flow
    const flowData = this.flowService.getAndCleanData('/dashboard');

    // Update iterator service
    if (flowData) {
      this.iteratorService = flowData.pagination;

      // We request the list of issues to the service, current page
      this.paginate(Pagination.Current);
    } else {
      this.iteratorService = new IteratorService(
        this.injector,
        AppConfig.services.issues.getListOfIssues);

      // We request the list of issues to the service, first page
      this.paginate(Pagination.First);
    }
  }

  /**
   * Navigate to issue page
   * @param issue
   */
  navigateToIssue(data: IssueModel): void {
    this.flowService.navigate('/issue', { data: data, pagination: this.iteratorService });
  }

  /**
   * Actions to do when removing the component
   */
  ngOnDestroy(): void {
    clearTimeout(this.timeout);
  }

  /**
   * Get pages
   */
  paginate(step: PaginationType): void {
    this.isReady = false;
    this.spinnerService.showSpinner();
    clearTimeout(this.timeout);

    this.timeout = setTimeout(() => {
      switch (step) {
        case Pagination.First:
          this.iteratorService.first()
            .map((data) => this.mapToIssueModel(data))
            .finally(() => this.finally())
            .subscribe((data: IssueModel[]) => {
              this.issuesList = data;
            },
            (error) => this.dialogService.showDialog(error.title, error.text));
          break;

        case Pagination.Prev:
          this.iteratorService.previous()
            .map((data) => this.mapToIssueModel(data))
            .finally(() => this.finally())
            .subscribe((data: IssueModel[]) => {
              this.issuesList = data;
            },
            (error) => this.dialogService.showDialog(error.title, error.text));
          break;

        case Pagination.Next:
          this.iteratorService.next()
            .map((data) => this.mapToIssueModel(data))
            .finally(() => this.finally())
            .subscribe((data: IssueModel[]) => {
              this.issuesList = data;
            },
            (error) => this.dialogService.showDialog(error.title, error.text));
          break;

        case Pagination.Last:
          this.iteratorService.last()
            .map((data) => this.mapToIssueModel(data))
            .finally(() => this.finally())
            .subscribe((data: IssueModel[]) => {
              this.issuesList = data;
            },
            (error) => this.dialogService.showDialog(error.title, error.text));
          break;

        case Pagination.Current:
          this.iteratorService.current()
            .map((data) => this.mapToIssueModel(data))
            .finally(() => this.finally())
            .subscribe((data: IssueModel[]) => {
              this.issuesList = data;
            },
            (error) => this.dialogService.showDialog(error.title, error.text));
          break;
      }
    }, AppConfig.timeout);
  }

  /**
   * Actions to do when pagination is finished
   */
  private finally(): void {
    this.isReady = true;
    this.spinnerService.hideSpinner();
  }

  /**
   * Map from entity to model
   */
  private mapToIssueModel(data: IssueEntity[]): IssueModel[] {
    let ret: IssueModel[];
    ret = [];

    if (data && data.length > 0) {
      for (let i = 0; i < data.length; i++) {
        ret.push(new IssueModel(data[i]));
      }
    }

    return ret;
  }
}
